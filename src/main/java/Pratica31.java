import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Marcelo G. Costa <marcelo@unicode.com.br>
 * Colaboração essencial de Alisson Comerlatto e Pedro Woiciechovski
 */
public class Pratica31 {
    private static String meuNome = "mArCeLo GuImArÃeS da COSTA";
    private static GregorianCalendar dataNascimento = new GregorianCalendar(1999, Calendar.SEPTEMBER, 21);
    private static Date inicio;
    private static Date fim;
    
    public static void main(String[] args){
        //printar o nome formatado
        //printar o numero de dias desde q vc nasceu
        //printar tempo levado para todo este processamento
        
        inicio = new Date();

        //printa o nome em letras maiúsculas 
        System.out.println(meuNome.toUpperCase());
        
        //Separa os nomes
        String[] nomes = meuNome.split(" ");
        
        // Pega o último sobrenome com a primeira letra maiuscula e o resto minuscula
        String nomeFormatado = 
                nomes[nomes.length-1].substring(0, 1).toUpperCase() 
                + nomes[nomes.length-1].substring(1).toLowerCase() + ", ";
        
        // Adiciona as iniciais dos outros nomes
        for(int i = 0; i < nomes.length - 1; i++)
        {
            nomeFormatado += nomes[i].substring(0, 1).toUpperCase() + ". ";
        }
        
        //remove espaçoes em branco no inicio e fim da string
        nomeFormatado = nomeFormatado.trim();
        
        //PRINT
        System.out.println(nomeFormatado);
        
        //inicio é date. inicio.getTime() é long
        //dataNascimento é GregorianCalendar. dataNascimento.getTime() é date. dataNascimento.getDate().geDate() é long
        //valores em milisegundos, divididos por 24*60*60*1000 (86400000), temos o valor em dias
        System.out.println((inicio.getTime() - dataNascimento.getTime().getTime())/86400000);
        
        fim = new Date();
        
        //printa a diferença entre inicio em fim, em milisegundos
        System.out.println(fim.getTime() - inicio.getTime());
    }
}
